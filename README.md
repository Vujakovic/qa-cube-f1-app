# qacube-frontend-bootcamp-2-bvujakovic
# F1 app

**This application represents a search engine for the database of formula one**

### Displays information about

* Races 
    * Race results
    * Qualification results
* Drivers
    * Drivers results

For the application functioning are necessary:
* angular.js
* angular-route.js

JS framework AngularJS was used to build this application.

**To start the application by running a f1app.bat file, it is necessary to have node.js and node live-server installed.**

**Another way to run is via Visual Studio Code LiveServer extension.**


