(function () {

    angular.module("myApp", ['ngRoute'])
        .config(function ($routeProvider) {
            $routeProvider.caseInsensitiveMatch = true;
            $routeProvider.errorOnUnhandledRejections = false;


            $routeProvider
                .when("/home", {
                    templateUrl: "templates/home.html"
                })
                .when("/races", {
                    templateUrl: "templates/races.html",
                    controller: "racesCtrl"
                })
                .when("/races/:id", {
                    templateUrl: "templates/raceDetail.html",
                    controller: "raceDetailCtrl"
                })
                .when("/drivers", {
                    templateUrl: "templates/drivers.html",
                    controller: "driversCtrl"
                })
                .when("/drivers/:id", {
                    templateUrl: "templates/driverDetail.html",
                    controller: "driverDetailCtrl"
                })
                .otherwise({
                    redirectTo: "/home"
                })
        });




})();

