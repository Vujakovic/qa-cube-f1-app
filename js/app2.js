
var logInBtn = document.getElementById('logInBtn');
var logOutBtn = document.getElementById('logOutBtn');
var signIn = document.querySelector('#signIn');
var userName = document.getElementById('userName');
var password = document.getElementById('password');
var navigation = document.querySelector('.navigation');
var chooseSeason = document.querySelector('#seasonPick');
var chooseBtn = document.querySelector('#choose');

userName.style.display = "inline-block";
password.style.display = "inline-block";
navigation.style.display = "none";
signIn.style.display = "inline-block";
logOutBtn.style.display = "none";
chooseSeason.style.display = "none";
seasonPick.style.display = "none";


class User {
	constructor(userName, password) {
		this.userName = userName;
		this.password = password;
	}
}

//USERS ARRAY
var users = [];

var admin = new User('admin', 'admin123');
users.push(admin);

// //EVENT LISTENER
signIn.addEventListener('click', addUser);
logInBtn.addEventListener('click', logIn);
logInBtn.addEventListener('', logIn);
logOutBtn.addEventListener('click', logOut);

//REG EXP
var regexUser = /^.*(?=.{10,})(?=.*\d)(?=.*[A-Z]+)(?=.*[a-zA-Z])(?=.*[@$!%*#?&]).*$/;
var regexPass = /^.*(?=.{6,})(?=.*\d)(?=.*[A-Z]+)(?=.*[a-zA-Z]).*$/;



// var provera = regexSeason.test('7444');
// if(provera){
// 	console.log('dobro je');
// }else{
// 	console.log('nije dobro');
// }



//FUNCTIONS
function addUser() {

	for (var i = 0; i < users.length; i++) {
		if (userName.value === users[i].userName) {
			document.querySelector('span').innerText = "User already exist";
			setTimeout(function () {
				document.querySelector('span').innerHTML = '';
			}, 3000);
			break;
		} else if (userName.value === "") {
			document.querySelector('span').innerText = "Please enter username";
			setTimeout(function () {
				document.querySelector('span').innerHTML = '';
			}, 3000);
			break;
		}
		else if (regexUser.test(userName.value) && regexPass.test(password.value)) {
			var newUser = new User(userName.value, password.value);
			users.push(newUser);
			console.log(users);
			localStorage.setItem('username', userName.value);
			localStorage.setItem('password', password.value);
			userName.value = "";
			password.value = "";
			document.querySelector('span').innerText = "User successfully added";
			setTimeout(function () {
				document.querySelector('span').innerHTML = '';
			}, 3000);
			break;
		} else if (!regexUser.test(userName.value) || !regexPass.test(password.value)) {
			document.querySelector('span').innerText = "Must contain min 10 characters, at least one digit and one special character";
			setTimeout(function () {
				document.querySelector('span').innerHTML = '';
			}, 3000);
			
			break;
		}
	}
}

function logIn() {
	for (var i = 0; i < users.length; i++) {
		if (userName.value == users[i].userName && password.value == users[i].password ||
			userName.value === localStorage.getItem('username') && password.value === localStorage.getItem('password')) {

			localStorage.setItem('username', userName.value);
			localStorage.setItem('password', password.value);
			location.replace('index.html#!/races');
			userName.value = "";
			password.value = "";
			logInBtn.style.display = "none";
			signIn.style.display = "none";
			logOutBtn.style.display = "inline-block";
			navigation.style.display = "block";
			userName.style.display = "none";
			password.style.display = "none";
			chooseSeason.style.display = "inline-block";
			seasonPick.style.display = "inline-block";
			localStorage.setItem("season", "2018");
			break;
		} else {
			document.querySelector('span').innerText = "Enter valid username and password";
			setTimeout(function () {
				document.querySelector('span').innerHTML = '';
			}, 3000);
		}
	}
}

function logOut() {
	localStorage.clear();
	location.replace('index.html#!/home');
	location.reload('index.html#!/home');

	userName.value = "";
	password.value = "";
	logOutBtn.style.display = "none";
	logInBtn.style.display = "inline-block";
	signin.style.display = "inline-block";
	navigation.style.display = "none";
	userName.style.display = "inline-block";
	password.style.display = "inline-block";


}

