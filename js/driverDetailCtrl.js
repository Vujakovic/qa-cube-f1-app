'use strict';

angular.module('myApp').controller('driverDetailCtrl', function ($scope, $http, $routeParams) {

    $scope.choosenSeason = localStorage.getItem("season");

    var driverId;
    driverId = $routeParams.id;

    var driver;
    driver = $routeParams.id;
    $http({
        url: 'http://ergast.com/api/f1/' + localStorage.getItem("season") + '/drivers/' + driver + '/driverStandings.json',
        method: 'get'
    }).then(function (response) {
        console.log(response.data.MRData.StandingsTable.StandingsLists[0].DriverStandings[0].Driver);
        $scope.driverData = response.data.MRData.StandingsTable.StandingsLists[0].DriverStandings[0].Driver;
    });


    $http({
        url: 'http://ergast.com/api/f1/' + localStorage.getItem("season") + '/drivers/' + driverId + '/results.json',
        method: 'get'
    }).then(function (response) {
        // console.log(response.data.MRData.RaceTable.Races);
        $scope.driverResults = response.data.MRData.RaceTable.Races;
    });


});
