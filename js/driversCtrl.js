'use strict';

angular.module('myApp').controller('driversCtrl', function ($scope, $http) {


    $scope.choosenSeason = localStorage.getItem("season");

    $http.get("http://ergast.com/api/f1/" + localStorage.getItem("season") + "/driverStandings.json")
        .then(function (response) {
            $scope.drivers = response.data.MRData.StandingsTable.StandingsLists[0].DriverStandings;
            //console.log($scope.drivers);
        });

});

