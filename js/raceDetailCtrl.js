'use strict';

angular.module('myApp').controller('raceDetailCtrl', function ($scope, $http, $routeParams) {

    var raceRound;
    raceRound = $routeParams.id;
    $scope.choosenSeason = localStorage.getItem("season");

    $http({
        url: "http://ergast.com/api/f1/" + localStorage.getItem("season") + "/" + raceRound + "/results.json",
        method: "get"
    })
        .then(function (response) {
            
            $scope.race = response.data.MRData.RaceTable.Races[0].Results;
            
        });

    var race;
    race = $routeParams.id;

    $http({
        url: "http://ergast.com/api/f1/" + localStorage.getItem("season") + "/" + race + "/qualifying.json",
        method: "get"
    })
        .then(function (response) {
           
            $scope.raceData = response.data.MRData.RaceTable.Races[0];
            $scope.qualifying = response.data.MRData.RaceTable.Races[0].QualifyingResults;

        
            //PRVA FUNKCIJA - NAJBOLJE KVALIFIKACIONO VREME PO VOZACU
            $scope.bestTime = function (index) {

                if($scope.qualifying[index].Q1 == ""){
                    $scope.qualifying[index].Q1="0:0";
                }


                if (typeof $scope.qualifying[index].Q3 !== "undefined"
                    && typeof $scope.qualifying[index].Q2 !== "undefined"
                    && typeof $scope.qualifying[index].Q1 !== "undefined") {


                    var q1 = parseFloat($scope.qualifying[index].Q1.split(':')[0] * 60 + parseFloat($scope.qualifying[index].Q1.split(':')[1]));
                    var q2 = parseFloat($scope.qualifying[index].Q2.split(':')[0] * 60 + parseFloat($scope.qualifying[index].Q2.split(':')[1]));
                    var q3 = parseFloat($scope.qualifying[index].Q3.split(':')[0] * 60 + parseFloat($scope.qualifying[index].Q3.split(':')[1]));

                    var firstInterval = q1;
                    var secondInterval = q2;
                    var thirdInterval = q3;


                    if (thirdInterval < secondInterval && thirdInterval < firstInterval) {
                        if (thirdInterval > 60) {
                            var minutes = Math.floor(thirdInterval / 60);
                            var seconds = thirdInterval - minutes * 60;
                            var timeFormat= minutes + ":" + seconds.toFixed(3);
                            return timeFormat;
                        }

                        
                    }
                    else if (secondInterval < firstInterval && secondInterval < thirdInterval) {
                        if (secondInterval > 60) {
                            var minutes = Math.floor(secondInterval / 60);
                            var seconds = secondInterval - minutes * 60;
                            var timeFormat= minutes + ":" + seconds.toFixed(3);
                            return timeFormat;
                        }
                        
                    }
                    else if (firstInterval < secondInterval && firstInterval < thirdInterval) {
                        if (firstInterval > 60) {
                            var minutes = Math.floor(firstInterval / 60);
                            var seconds = firstInterval - minutes * 60;
                            var timeFormat= minutes + ":" + seconds.toFixed(3);
                            return timeFormat;
                        }
                        
                    }

                }
                else if (typeof $scope.qualifying[index].Q3 == "undefined"
                    && typeof $scope.qualifying[index].Q2 !== "undefined"
                    && typeof $scope.qualifying[index].Q1 !== "undefined") {


                    var q1 = parseFloat($scope.qualifying[index].Q1.split(':')[0] * 60 + parseFloat($scope.qualifying[index].Q1.split(':')[1]));
                    var q2 = parseFloat($scope.qualifying[index].Q2.split(':')[0] * 60 + parseFloat($scope.qualifying[index].Q2.split(':')[1]));


                    var firstInterval = q1;
                    var secondInterval = q2;


                    if (firstInterval < secondInterval) {
                        if (firstInterval > 60) {
                            var minutes = Math.floor(firstInterval / 60);
                            var seconds = firstInterval - minutes * 60;
                            var timeFormat= minutes + ":" + seconds.toFixed(3);
                        }
                        return timeFormat;
                    }
                    else if (secondInterval < firstInterval) {
                        if (secondInterval > 60) {
                            var minutes = Math.floor(secondInterval / 60);
                            var seconds = secondInterval - minutes * 60;
                            var timeFormat= minutes + ":" + seconds.toFixed(3);
                            return timeFormat;
                        }
                        
                    }
                }
                else if (typeof $scope.qualifying[index].Q3 == "undefined"
                    && typeof $scope.qualifying[index].Q2 == "undefined"
                    && typeof $scope.qualifying[index].Q1 !== "undefined") {

                    var q1 = parseFloat($scope.qualifying[index].Q1.split(':')[0] * 60 + parseFloat($scope.qualifying[index].Q1.split(':')[1]));

                    var firstInterval = q1;

                    if (firstInterval > 60) {
                        var minutes = Math.floor(firstInterval / 60);
                        var seconds = firstInterval - minutes * 60;
                        var timeFormat= minutes + ":" + seconds.toFixed(3);
                        return timeFormat;
                    }

                   
                }

            }


            // DRUGA FUNKCIJA - NAJLOSIJE KVALIFIKACIONO VREME PO VOZACU
            $scope.slowestLap = function (index) {

                if (typeof $scope.qualifying[index].Q3 !== "undefined"
                    && typeof $scope.qualifying[index].Q2 !== "undefined"
                    && typeof $scope.qualifying[index].Q1 !== "undefined") {


                    var q1 = parseFloat($scope.qualifying[index].Q1.split(':')[0] * 60 + parseFloat($scope.qualifying[index].Q1.split(':')[1]));
                    var q2 = parseFloat($scope.qualifying[index].Q2.split(':')[0] * 60 + parseFloat($scope.qualifying[index].Q2.split(':')[1]));
                    var q3 = parseFloat($scope.qualifying[index].Q3.split(':')[0] * 60 + parseFloat($scope.qualifying[index].Q3.split(':')[1]));

                    var firstInterval = parseFloat(q1);
                    var secondInterval = parseFloat(q2);
                    var thirdInterval = parseFloat(q3);


                    if (thirdInterval > secondInterval && thirdInterval > firstInterval) {
                        if (thirdInterval > 60) {
                            var minutes = Math.floor(thirdInterval / 60);
                            var seconds = thirdInterval - minutes * 60;
                            var timeFormat= minutes + ":" + seconds.toFixed(3);
                        }
                        return timeFormat;
                    }
                    else if (secondInterval > firstInterval && secondInterval > thirdInterval) {
                        if (secondInterval > 60) {
                            var minutes = Math.floor(secondInterval / 60);
                            var seconds = secondInterval - minutes * 60;
                            var timeFormat= minutes + ":" + seconds.toFixed(3);
                        }
                        return timeFormat;
                    }
                    else if (firstInterval > secondInterval && firstInterval > thirdInterval) {
                        if (firstInterval > 60) {
                            var minutes = Math.floor(firstInterval / 60);
                            var seconds = firstInterval - minutes * 60;
                            var timeFormat= minutes + ":" + seconds.toFixed(3);
                        }
                        return timeFormat;
                    }

                }
                else if (typeof $scope.qualifying[index].Q3 == "undefined"
                    && typeof $scope.qualifying[index].Q2 !== "undefined"
                    && typeof $scope.qualifying[index].Q1 !== "undefined") {


                    var q1 = parseFloat($scope.qualifying[index].Q1.split(':')[0] * 60 + parseFloat($scope.qualifying[index].Q1.split(':')[1]));
                    var q2 = parseFloat($scope.qualifying[index].Q2.split(':')[0] * 60 + parseFloat($scope.qualifying[index].Q2.split(':')[1]));

                    var firstInterval = q1;
                    var secondInterval = q2;



                    if (firstInterval > secondInterval) {
                        if (firstInterval > 60) {
                            var minutes = Math.floor(firstInterval / 60);
                            var seconds = firstInterval - minutes * 60;
                            var timeFormat= minutes + ":" + seconds.toFixed(3);
                        }
                        return timeFormat;
                    }
                    else if (secondInterval > firstInterval) {
                        if (secondInterval > 60) {
                            var minutes = Math.floor(secondInterval / 60);
                            var seconds = secondInterval - minutes * 60;
                            var timeFormat= minutes + ":" + seconds.toFixed(3);
                        }
                        return timeFormat;
                    }
                }
                else if (typeof $scope.qualifying[index].Q3 == "undefined"
                    && typeof $scope.qualifying[index].Q2 == "undefined"
                    && typeof $scope.qualifying[index].Q1 !== "undefined") {

                    var q1 = parseFloat($scope.qualifying[index].Q1.split(':')[0] * 60 + parseFloat($scope.qualifying[index].Q1.split(':')[1]));

                    var firstInterval = q1;

                    if (firstInterval > 60) {
                        var minutes = Math.floor(firstInterval / 60);
                        var seconds = firstInterval - minutes * 60;
                        var timeFormat= minutes + ":" + seconds.toFixed(3);
                    }
                    return timeFormat;
                }
            }



            //TRECA FUNKCIJA - PROSECNO VREME KVALIFIKACIJA PO VOZACU 
            $scope.averageTime = function (index) {

                var finalSum;
                if($scope.qualifying[index].Q1 === ""){
                    this.qualifying[index].Q1="0:0";
                }

                if (typeof $scope.qualifying[index].Q3 !== "undefined"
                    && typeof $scope.qualifying[index].Q2 !== "undefined"
                    && typeof $scope.qualifying[index].Q1 !== "undefined") {


                    var q1 = parseFloat($scope.qualifying[index].Q1.split(':')[0] * 60 + parseFloat($scope.qualifying[index].Q1.split(':')[1]));
                    var q2 = parseFloat($scope.qualifying[index].Q2.split(':')[0] * 60 + parseFloat($scope.qualifying[index].Q2.split(':')[1]));
                    var q3 = parseFloat($scope.qualifying[index].Q3.split(':')[0] * 60 + parseFloat($scope.qualifying[index].Q3.split(':')[1]));


                    finalSum = (q1 + q2 + q3) / 3;

                    if (finalSum > 60) {
                        var minutes = Math.floor(finalSum / 60);
                        var seconds = finalSum - minutes * 60;
                        var timeFormat= minutes + ":" + seconds.toFixed(3);
                    }
                    return timeFormat;
                }
                else if (typeof $scope.qualifying[index].Q3 == "undefined"
                    && typeof $scope.qualifying[index].Q2 !== "undefined"
                    && typeof $scope.qualifying[index].Q1 !== "undefined") {


                    var q1 = parseFloat($scope.qualifying[index].Q1.split(':')[0] * 60 + parseFloat($scope.qualifying[index].Q1.split(':')[1]));
                    var q2 = parseFloat($scope.qualifying[index].Q2.split(':')[0] * 60 + parseFloat($scope.qualifying[index].Q2.split(':')[1]));

                    finalSum = (q1 + q2) / 2;


                    if (finalSum > 60) {
                        var minutes = Math.floor(finalSum / 60);
                        var seconds = finalSum - minutes * 60;
                        var timeFormat= minutes + ":" + seconds.toFixed(3);
                    }
                    return timeFormat;
                }
                else if (typeof $scope.qualifying[index].Q3 == "undefined"
                    && typeof $scope.qualifying[index].Q2 == "undefined"
                    && typeof $scope.qualifying[index].Q1 !== "undefined") {


                    var q1 = parseFloat($scope.qualifying[index].Q1.split(':')[0] * 60 + parseFloat($scope.qualifying[index].Q1.split(':')[1]));

                    finalSum = q1;

                    if (finalSum > 60) {
                        var minutes = Math.floor(finalSum / 60);
                        var seconds = finalSum - minutes * 60;
                        var timeFormat= minutes + ":" + seconds.toFixed(3);
                    }
                    return timeFormat;
                    
                }

            }


            //CETVRTA FUNKCIJA - PROSECNO VREME KVALIFIKACIJA PO TRCI

            $scope.averageTimeQ = function () {
                var qualifying = response.data.MRData.RaceTable.Races[0].QualifyingResults;
                var finalSum;
                var sumAverageTime=0;
                
                for (var i = 0; i < qualifying.length; i++) {

                    if(qualifying[i].Q1 === ""){
                        this.qualifying[i].Q1="0:0";
                    }

                    if (typeof $scope.qualifying[i].Q3 !== "undefined"
                    && typeof $scope.qualifying[i].Q2 !== "undefined"
                    && typeof $scope.qualifying[i].Q1 !== "undefined") {
                   
                    var q1 = parseFloat($scope.qualifying[i].Q1.split(':')[0] * 60 + parseFloat($scope.qualifying[i].Q1.split(':')[1]));
                    var q2 = parseFloat($scope.qualifying[i].Q2.split(':')[0] * 60 + parseFloat($scope.qualifying[i].Q2.split(':')[1]));
                    var q3 = parseFloat($scope.qualifying[i].Q3.split(':')[0] * 60 + parseFloat($scope.qualifying[i].Q3.split(':')[1]));
                    

                    finalSum = (q1 + q2 + q3) / 3;
                    sumAverageTime+= finalSum;
                    
                }
                else if (typeof $scope.qualifying[i].Q3 == "undefined"
                    && typeof $scope.qualifying[i].Q2 !== "undefined"
                    && typeof $scope.qualifying[i].Q1 !== "undefined") {


                    var q1 = parseFloat($scope.qualifying[i].Q1.split(':')[0] * 60 + parseFloat($scope.qualifying[i].Q1.split(':')[1]));
                    var q2 = parseFloat($scope.qualifying[i].Q2.split(':')[0] * 60 + parseFloat($scope.qualifying[i].Q2.split(':')[1]));

                    finalSum = (q1 + q2) / 2;
                    sumAverageTime += finalSum;

                    
                }
                else if (typeof $scope.qualifying[i].Q3 == "undefined"
                    && typeof $scope.qualifying[i].Q2 == "undefined"
                    && typeof $scope.qualifying[i].Q1 !== "undefined") {


                    var q1 = parseFloat($scope.qualifying[i].Q1.split(':')[0] * 60 + parseFloat($scope.qualifying[i].Q1.split(':')[1]));

                    finalSum = q1;
                    sumAverageTime += finalSum;
                }
                }
                
                var averageQ = sumAverageTime / qualifying.length;

                 
                if (averageQ > 60) {
                    var minutes = Math.floor(averageQ / 60);
                    var seconds = averageQ - minutes * 60;
                    var timeFormat= minutes + ":" + seconds.toFixed(3);
                    // console.log(timeFormat);
                    return timeFormat;
                }


            }


            //PETA FUNKCIJA - NAJBOLJE VREME KVALIFIKACIJA PO TRCI

            $scope.bestTimeQ = function () {
                var qualifying = response.data.MRData.RaceTable.Races[0].QualifyingResults;

                var sumBestTime = 0;
                for (var i = 0; i < qualifying.length; i++) {

                    if (typeof $scope.qualifying[i].Q3 !== "undefined"
                        && typeof $scope.qualifying[i].Q2 !== "undefined"
                        && typeof $scope.qualifying[i].Q1 !== "undefined") {

                        var q1 = parseFloat($scope.qualifying[i].Q1.split(':')[0] * 60 + parseFloat($scope.qualifying[i].Q1.split(':')[1]));
                        var q2 = parseFloat($scope.qualifying[i].Q2.split(':')[0] * 60 + parseFloat($scope.qualifying[i].Q2.split(':')[1]));
                        var q3 = parseFloat($scope.qualifying[i].Q3.split(':')[0] * 60 + parseFloat($scope.qualifying[i].Q3.split(':')[1]));

                        var firstInterval = q1;
                        var secondInterval = q2;
                        var thirdInterval = q3;


                        if (thirdInterval < secondInterval && thirdInterval < firstInterval) {
                            if (thirdInterval > 60) {
                                sumBestTime += thirdInterval;
                            }

                        }
                        else if (secondInterval < firstInterval && secondInterval < thirdInterval) {
                            if (secondInterval > 60) {
                                sumBestTime += secondInterval;
                            }

                        }
                        else if (firstInterval < secondInterval && firstInterval < thirdInterval) {
                            if (firstInterval > 60) {
                                sumBestTime += firstInterval;
                            }

                        }

                    }
                    else if (typeof $scope.qualifying[i].Q3 == "undefined"
                        && typeof $scope.qualifying[i].Q2 !== "undefined"
                        && typeof $scope.qualifying[i].Q1 !== "undefined") {

                        var q1 = parseFloat($scope.qualifying[i].Q1.split(':')[0] * 60 + parseFloat($scope.qualifying[i].Q1.split(':')[1]));
                        var q2 = parseFloat($scope.qualifying[i].Q2.split(':')[0] * 60 + parseFloat($scope.qualifying[i].Q2.split(':')[1]));

                        var firstInterval = q1;
                        var secondInterval = q2;

                        if (firstInterval < secondInterval) {
                            if (firstInterval > 60) {
                                sumBestTime += firstInterval;
                            }
                        }
                        else if (secondInterval < firstInterval) {
                            if (secondInterval > 60) {
                                sumBestTime += secondInterval;
                            }
                        }
                    }
                    else if (typeof $scope.qualifying[i].Q3 == "undefined"
                        && typeof $scope.qualifying[i].Q2 == "undefined"
                        && typeof $scope.qualifying[i].Q1 !== "undefined") {

                        var q1 = parseFloat($scope.qualifying[i].Q1.split(':')[0] * 60 + parseFloat($scope.qualifying[i].Q1.split(':')[1]));

                        var firstInterval = q1;

                        if (firstInterval > 60) {
                            sumBestTime += firstInterval;
                        }
                    }

                     var averageQ = sumBestTime / qualifying.length;
                }
                
                if (averageQ > 60) {
                    var minutes = Math.floor(averageQ / 60);
                    var seconds = averageQ - minutes * 60;
                    var timeFormat= minutes + ":" + seconds.toFixed(3);
                    return timeFormat;
                }
            }


            $scope.slowestTimeQ = function(){
                var qualifying = response.data.MRData.RaceTable.Races[0].QualifyingResults;

                var sumSlowestTime = 0;
                for (var i = 0; i < qualifying.length; i++) {

                    if (typeof $scope.qualifying[i].Q3 !== "undefined"
                        && typeof $scope.qualifying[i].Q2 !== "undefined"
                        && typeof $scope.qualifying[i].Q1 !== "undefined") {

                        var q1 = parseFloat($scope.qualifying[i].Q1.split(':')[0] * 60 + parseFloat($scope.qualifying[i].Q1.split(':')[1]));
                        var q2 = parseFloat($scope.qualifying[i].Q2.split(':')[0] * 60 + parseFloat($scope.qualifying[i].Q2.split(':')[1]));
                        var q3 = parseFloat($scope.qualifying[i].Q3.split(':')[0] * 60 + parseFloat($scope.qualifying[i].Q3.split(':')[1]));

                        var firstInterval = q1;
                        var secondInterval = q2;
                        var thirdInterval = q3;


                        if (thirdInterval > secondInterval && thirdInterval > firstInterval) {
                            
                                sumSlowestTime += thirdInterval;
                           
                        }
                        else if (secondInterval > firstInterval && secondInterval > thirdInterval) {
                            
                            sumSlowestTime += secondInterval;
                        }
                        else if (firstInterval > secondInterval && firstInterval > thirdInterval) {
                           
                            sumSlowestTime += firstInterval;
                        }

                    }
                    else if (typeof $scope.qualifying[i].Q3 == "undefined"
                        && typeof $scope.qualifying[i].Q2 !== "undefined"
                        && typeof $scope.qualifying[i].Q1 !== "undefined") {

                        var q1 = parseFloat($scope.qualifying[i].Q1.split(':')[0] * 60 + parseFloat($scope.qualifying[i].Q1.split(':')[1]));
                        var q2 = parseFloat($scope.qualifying[i].Q2.split(':')[0] * 60 + parseFloat($scope.qualifying[i].Q2.split(':')[1]));

                        var firstInterval = q1;
                        var secondInterval = q2;

                        if (firstInterval > secondInterval) {
                           
                            sumSlowestTime += firstInterval;
                        }
                        else if (secondInterval > firstInterval) {
                           
                            sumSlowestTime += secondInterval;
                        }
                    }
                    else if (typeof $scope.qualifying[i].Q3 == "undefined"
                        && typeof $scope.qualifying[i].Q2 == "undefined"
                        && typeof $scope.qualifying[i].Q1 !== "undefined") {

                        var q1 = parseFloat($scope.qualifying[i].Q1.split(':')[0] * 60 + parseFloat($scope.qualifying[i].Q1.split(':')[1]));

                        var firstInterval = q1;

                        sumSlowestTime += firstInterval;
                    }
                    // console.log(sumSlowestTime);
                     var averageQ = sumSlowestTime / qualifying.length;
                }
                
                if (averageQ > 60) {
                    var minutes = Math.floor(averageQ / 60);
                    var seconds = averageQ - minutes * 60;
                    var timeFormat= minutes + ":" + seconds.toFixed(3);
                    return timeFormat;
                }
            }


        });






});



