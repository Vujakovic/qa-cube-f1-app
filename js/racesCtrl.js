'use strict';
angular.module('myApp').controller('racesCtrl', function ($scope, $http) {


    var season;

    document.querySelector('#seasonPick').addEventListener('change', seasonChange);

    $http.get('http://ergast.com/api/f1/' + localStorage.getItem("season") + '/results/1.json')
        .then(function (response) {
            $scope.races = response.data.MRData.RaceTable.Races;
            //console.log(response.data.MRData.RaceTable.Races);
        });

    $scope.choosenSeason = localStorage.getItem("season", season);


    function seasonChange() {
        var select = document.querySelector('#seasonPick')
        season = select.options[select.selectedIndex].value;
        localStorage.setItem("season", season);

        $scope.choosenSeason = localStorage.getItem("season");

        location.replace('index.html#!/races');

    }


    var niz = [];
    var niz2 = [];
    var sum = 0;
    var q1;
    var averageQtime;
    var timeFormat;



    //PROSEČNO VREME SEZONE
    $http.get("http://ergast.com/api/f1/" + localStorage.getItem("season") + "/races.json")
    .then(function (response) {
       $scope.numberOfRaces= parseInt(response.data.MRData.total);
       //console.log($scope.numberOfRaces);
    })

    $http.get("http://ergast.com/api/f1/" + localStorage.getItem("season") + "/qualifying.json")
        .then(function (response) {

            $scope.totalData = parseFloat(response.data.MRData.total);
            $scope.limitData = response.data.MRData.RaceTable.Races[0].QualifyingResults.length;
           
            //console.log($scope.totalData + " Broj vozaca * broj trka");
            //console.log($scope.limitData + " Broj dostupnih podataka API-ja po pozivu");
        })
        .then(function () {
            //var niz = [];
            for (var i = 0; i < $scope.totalData; i += $scope.limitData) {

                var url = "http://ergast.com/api/f1/" + localStorage.getItem("season") + "/qualifying.json?limit=" + $scope.limitData + "&offset=";

                $http.get(url + i)
                    .then(function (response) {
                        var results = response.data.MRData.RaceTable.Races;
                        //console.log(results);
                        //console.log($scope.totalData);

                        for (var i = 0; i < results.length; i++) {
                            var drivers = results[i].QualifyingResults;
                            //console.log(drivers);

                                    for (var i = 0; i < drivers.length; i++) {
                                        //console.log(drivers.length);
                                        if (drivers[i].Q1 == "undefined") {
                                            drivers[i].Q1 = "0:0";
                                            $scope.totalData-1;
                                        } else if (drivers[i].Q1 == "") {
                                            drivers[i].Q1 = "0:0";
                                            $scope.totalData - 1;
                                        }

                                        q1 = parseFloat(drivers[i].Q1.split(':')[0] * 60 + parseFloat(drivers[i].Q1.split(':')[1]));
                                        
                                        
                                        if(q1>0){
                                        niz2.push(q1);
                                        sum += q1;
                                        $scope.niz2length=niz2.length;
                                        }
                                        
                                        if (i == drivers.length - 1) {
                                            //console.log(drivers[i].Q1);

                                            averageQtime = sum / $scope.niz2length;

                                            niz.push(averageQtime);
                                            //console.log(averageQtime);
                                            $scope.average = parseFloat(niz[$scope.numberOfRaces-1]);
                                            if ($scope.average > 60) {
                                                var minutes = Math.floor($scope.average / 60);
                                                var seconds = $scope.average - minutes * 60;
                                                timeFormat = minutes + ":" + seconds.toFixed(3);
                                            }
                                                $scope.timeFormat = timeFormat;
                                            //console.log(averageQtime); //prosek trke podeljen brojem vozaca i brojem trka, pa sabran sa sledecim prosekom trke  
                                            //console.log(niz[$scope.numberOfRaces-1]);
                                        }
                                         
                                    }
                        }
                    });
            }
            
            
        });
        
//console.log(niz);
console.log(niz2);


});

